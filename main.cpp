#include <iostream>
#include <list>
#include <math.h>
#include <allegro.h>


using namespace std;

BITMAP * page;

//*****************************************
//**** classe forme du poly de cours ******
//******** classes du TD4 *****************
//*****************************************

class forme
{
public:
    forme();
    virtual ~forme();
    virtual void affiche()= 0;
};

class listeforme
{  protected :
    list <forme*> l;
    list<forme*>::iterator it;
public :
    listeforme();
    ~listeforme();
    void ajoute(forme*);
    void supprime(forme*);
    void affiche();
    int fsize() { return l.size(); }
};

//************** classes du TD3 *************
class point
{  protected :
      float x,y;
   public :
     point();
     point(float,float);
     ~point();
     void deplace(float, float);
     void init(float,float);
     void affiche();
     float getx() { return x; }
     float gety() { return y; }
};

class rectangle:public forme
{  protected :
      point p1,p2;
   public :
     rectangle();
     rectangle(float, float, float, float);
     ~rectangle();
     void init(float, float, float, float);
     point getp1() { return p1;}
     point getp2() { return p2;}
     bool selection(float,float);
};

class brique:public rectangle
{  protected :
      long coul;
      int nbcollision;
   public :
     brique();
     brique(float, float, float, float,long,int);
     ~brique() {}
     void affiche();
     // virtual int action()=0;
};

class balle:public forme
{  protected :
      point p;
      float rayon,pasx,pasy;
   public :
     balle();
     balle(float, float, float, float);
     ~balle() {}
     void affiche();
     void direction(int);
     void deplace();
     bool selection(float ,float );
     int testlimite();
};

class murbrique:public forme
{  protected :
    list <brique*> l;
    list<brique*>::iterator it;
public :
    murbrique();
    ~murbrique();
    void affiche();
};

//************ FORME ****************
forme::forme() { }

forme::~forme() { }

//*********** POINT *****************
point::point() { x=0; y=0; }

point::point(float px,float py)
{ x=px;  y=py;     }

point::~point() {}

void point::deplace(float dx, float dy)
{    x=x+dx; y=y+dy;  }

void point::init(float px,float py)
{ x=px;  y=py;     }

void point::affiche() {  }

//****** rectangle ************
rectangle::rectangle():p1(),p2() { }

rectangle::rectangle(float px,float py,float pl,float ph):p1(px,py),p2(px+pl,py+ph)
{ }

rectangle::~rectangle() { }

void rectangle::init(float px1,float py1,float px2,float py2)
{   p1.init(px1,py1);
    p2.init(px2,py2);
}

bool rectangle::selection(float sx,float sy)
{   if ( (sx >= p1.getx()) && (sx <= p2.getx()) && (sy >= p1.gety()) && (sy <= p2.gety()) ) return true;
    return false;
}

// ************ LISTEFORME ***********
listeforme::listeforme() {  }

listeforme::~listeforme() {  }

void listeforme::affiche()
{    for(it=l.begin(); it!=l.end(); it++)
        (*it)->affiche();
}

void listeforme::ajoute(forme* p)
{        l.push_back(p);  }

void listeforme::supprime(forme* p)
{       delete p;         l.remove(p);  }

//********** BRIQUE ***********************
brique::brique():rectangle()
{   coul=makecol(0,255,0);
    nbcollision = 1;
}

brique::brique(float px, float py, float pl, float ph,long pcoul,int pnbcol):rectangle(px,py,pl,ph)
{   coul=pcoul;
    nbcollision = pnbcol;
}

void brique::affiche()
{    rectfill(page,p1.getx(),p1.gety(),p2.getx(),p2.gety(),coul);
}

//************ BALLE **********************
balle::balle():p(200,200),rayon(10)
{ pasx=pasy=0;  }

balle::balle(float px,float py,float pr,float ppas):p(px,py),rayon(pr)
{ pasx=pasy=ppas;  }

void balle::affiche()
{ circlefill(page,p.getx(),p.gety(),rayon,makecol(255,0,0));   }

int balle::testlimite()
{
    if (p.getx() - rayon + pasx <= 0) return 1;
    if (p.getx() + rayon + pasx >= SCREEN_W)  return 3;
    if (p.gety() - rayon + pasy <= 0) return 2;
    if (p.gety() + rayon + pasy >= SCREEN_H) return 4;
    return 0;
}

void balle::direction(int pface)
{  if ( (pface == 1) || (pface == 3) ) pasx=-pasx;
   if ( (pface == 2) || (pface == 4) ) pasy=-pasy;
}

void balle::deplace()
{    p.deplace(pasx,pasy);  }

//*************** MURBRIQUE ********************
murbrique::murbrique()
{   float x,y;
    int i,j;
    for (x=50,i=1;i<=6;i++,x=x+100)
        for (y=40,j=1;j<=3;j++,y=y+70)
               l.push_back(new brique(x,y,45,30,makecol(100,0,255),8) );

}

murbrique::~murbrique()
{    for(it=l.begin(); it!=l.end(); it++)
        delete (*it);
}

void murbrique::affiche()
{    for(it=l.begin(); it!=l.end(); it++)
        (*it)->affiche();
}

//************ initialisation fen�tre graphique *************
void initfenetre()
{   allegro_init();
    install_keyboard();   //installe le clavier
    install_mouse();      //installe la souris

    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED,640,480,0,0)!=0)
    {   //initialisation - r�solution de l'�cran (tailles 640-480)
      		allegro_message(allegro_error);
            //allegro_message("mode graphique") ;
      		allegro_exit();
      		return;
    }

    set_mouse_sprite(NULL);
    show_mouse(screen);
    set_keyboard_rate(0,0);

	page=create_bitmap(SCREEN_W,SCREEN_H);

   	if ( !page )
	{       allegro_message("pb bitmap") ;
      		allegro_exit();
      		return;
    }
}

int main()
{ listeforme l;
  balle *b;
  murbrique *mb;

  cout << "d�but prg" << endl;

  initfenetre();

  l.ajoute( mb = new murbrique );
  l.ajoute( b = new balle(300,300,7,0.2) );

  do
  {
        clear_bitmap(page);

        b->direction( b->testlimite());

        b->deplace();

        l.affiche();

        blit(page,screen,0,0,0,0,page->w,page->h);
    }
    while(!key[KEY_ESC]);

    destroy_bitmap(page);
    set_gfx_mode(GFX_TEXT,0,0,0,0);

    cout << "fin prg" << endl;

    return 0;

} END_OF_MAIN() ;
